//Bước 1: Khai báo thư viện mongoose
const mongoose = require("mongoose");

//Bước 2: Khai báo thư viện Schema
const Schema = mongoose.Schema;

//Bước 3: Tạo ra 1 đối tượng Schema tương ứng với 1 collection trong mongodb
const userSchema = new Schema({
    __id: {
        type: mongoose.Types.ObjectId,
        unique: true
    },
    fullName: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    address: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: true,
        unique: true
    },
    orders: [
        {
            type: mongoose.Types.ObjectId,
            ref: "Order"
        }
    ],
    ngayTao: {
        type: Date,
        default: Date.now()
    },
    ngayCapNhat: {
        type: Date,
        default: Date.now()
    }
});
//Bước 4: Export ra 1 model cho schema
module.exports = mongoose.model("User", userSchema);
/*
User: {
	_id: ObjectId, unique
	fullName: String, required
	email: String, required, unique
address: String, required
phone: String, required, unique
orders: Array[ObjectId], ref: Order
	ngayTao: Date, default: Date.now()
ngayCapNhat: Date, default: Date.now()
}
*/