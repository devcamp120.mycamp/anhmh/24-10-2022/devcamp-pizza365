//Bước 1: Khai báo thư viện mongoose
const mongoose = require("mongoose");

//Bước 2: Khai báo thư viện Schema
const Schema = mongoose.Schema;

//Bước 3: Tạo ra 1 đối tượng Schema tương ứng với 1 collection trong mongodb
const drinkSchema = new Schema({
    __id: {
        type: mongoose.Types.ObjectId,
        unique: true
    },
    maNuocUong: {
        type: String,
        unique: true,
        required: true
    },
    tenNuocUong: {
        type: String,
        required: true
    },
    donGia: {
        type: Number,
        required: true
    },
    ngayTao: {
        type: Date,
        default: Date.now()
    },
    ngayCapNhat: {
        type: Date,
        default: Date.now()
    }
}); 

//Bước 4: Export ra 1 model cho schema
module.exports = mongoose.model("Drink", drinkSchema);

/*

Drink: {
	_id: ObjectId, unique
	maNuocUong: String, unique, required
	tenNuocUong: String, required
	donGia: Number, required,
	ngayTao: Date, default: Date.now()
ngayCapNhat: Date, default: Date.now()
}

*/