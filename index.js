//Câu lệnh này tương tự import express from 'express'; Dùng để import thư viện vào project
const express = require("express");

//Khai báo thư viện Mongoose
const mongoose = require('mongoose');

//Kết nối đến cơ sở dữ liệu Mongoose
mongoose.connect('mongodb://localhost:27017/CRUD_Pizza365', (error) => {
    if (error) {
        throw error;
    }
    console.log("Successfully connected!");
});

//Khai báo các model 
const drinkModel = require("./app/models/drinkModel");
const voucherModel = require("./app/models/voucherModel");
const orderModel = require("./app/models/orderModel");
const userModel = require("./app/models/userModel");

//Khai báo thư viện path
const path = require("path");

//Khởi tạo app express
const app = express();

//Khai báo cổng của project 
const port = 8000;

//Khai báo sử dụng các tài nguyên static (images, js, css v...v...)
app.use(express.static("views"));

//Sử dụng được body json
app.use(express.json());

//Sử dụng body unicode
app.use(express.urlencoded({
    extended:true
}))

//Khai báo API dạng GET "/" sẽ chạy vào đây
//Callback function là một tham số của hàm khác và nó sẽ được thực thi ngay sau khi hàm đấy được gọi
app.get("/pizza365index", (request, response) => {
    console.log(__dirname);
    response.sendFile(path.join(__dirname + '/views/pizza365index.html'));
})

//Chạy app express
app.listen(port, () => {
    console.log("App listening on port (Ứng dụng đang chạy trên cổng)" + port);
})